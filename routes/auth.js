/**
 * Created by lopezjoe on 5/30/2015.
 */

var express = require('express');
var cps = require('cps-api');
var uuid = require('node-uuid');
var app = express();

// TODO use public & private key

app.post('/register', function(req, res) {
    var user = req.body.username.toLowerCase();
    var pass = req.body.password;
    var mail = req.body.email;
    var gcm = req.body.gcm;
    var conn = global.cps_connection;

    var insert = [{
        "id": new Date().getMilliseconds(),
        "username": user,
        "passwords": pass,
        "email": mail,
        "avatar": ""
    }];

    var insert_req = new cps.InsertRequest(insert);
    conn.sendRequest(insert_req, function(err, resp) {
        if (err) {
            console.error(err);
            if (err[0].code == "2626")
                global.show_error("Username already exists", 409, res, false);
            else
                global.show_error(err, 500, res, false);
        }
        else {
            res.statusCode = 201;
            answerToken(res,user,gcm);
        }
    });
});

app.post('/login', function(req, res) {
    var user = req.body.username.toLowerCase();
    var pass = req.body.password;
    var gcm = req.body.gcm;
    var conn = global.cps_connection;
    var search_req = new cps.SearchRequest("{" + cps.Term(user, "username"));
    conn.sendRequest(search_req, function(err, search_resp) {
        if (err) {
            global.show_error(err, 500, res, true);
        }
        else {
            if (search_resp.hasOwnProperty("results") && pass == search_resp.results.document[0].passwords)
                answerToken(res,user,gcm);
            else
                global.show_error("Incorrect username or password", 403, res, false);
        }
    });
});

// TODO rename with a more convenient name
function answerToken(res, user,gcm) {
    var token = uuid.v1().split("-").join("_");

    var upd = {
        username: user,
        access_token: token,
        gcm: gcm
    };
    var replace_request = new cps.PartialReplaceRequest(upd);
    global.cps_connection.sendRequest(replace_request, function(err, repl) {
        if (err)
            global.show_error(err, 504, res, true);
        else
            res.json({access_token: token});
    });
}

module.exports = app;