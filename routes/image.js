/**
 * Created by lopezjoe on 5/31/2015.
 */
var express = require('express');
var cps = require('cps-api');
var multiparty = require('multiparty');
var mv = require('mv');
var uid = require('uid');
var gcm = require('node-gcm');

var router = express.Router();
var app = express();

app.get('/:id', function(req, res) {
    res.sendFile('/data/'+req.params.id+'.jpg', {
        root: __dirname + '/public',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    }, function (err) {
        if (err) {
            res.status(404).end();
        } // TODO Remove photos
    });
});

app.post('/', function(req, res) {
    var form = new multiparty.Form();
    var conn = global.cps_connection;
    form.parse(req, function(err, fields, files) {
        if (err)
            global.show_error(err, 500, res, true);
        else if (!files)
            global.show_error("No files in multipart", 500, res, true);
        else if (req.query.hasOwnProperty("access_token")) {
            var token = req.query["access_token"];
            var search_req = new cps.SearchRequest("{" + cps.Term('"' + token + '"', "access_token"));
            conn.sendRequest(search_req, function (err, resp) {
                if (err)
                    global.show_error(err, 500, res, false);
                else if (resp.hasOwnProperty("results")) {
                    var img_token = uid(5);
                    console.log(files.image[0]);
                    mv(files.image[0].path, __dirname + '/public/data/' + img_token + '.jpg', {mkdirp: true}, function (err) {
                        if (err)
                            global.show_error("Error moving image", 500, res, false);
                        else {
                            var message = new gcm.Message();

                            message.addData('from', resp.results.document[0].username);
                            //message.addData('img', img_token);
                            var search_req = new cps.SearchRequest("{" + cps.Term('"' + req.query.receiver + '"', "username"));
                            global.cps_connection.sendRequest(search_req, function(err, resp) {
                                if (err) global.show_error(err, 500, res, true);
                                else {
                                    var receiver = resp.results.document[0].gcm;
                                    var regIds = [];
                                    regIds.push(receiver);
                                    console.log(receiver);
                                    var sender = new gcm.Sender('AIzaSyBLvNaq0riGNdaBwNDNRjb2wImAVTA59O0');
                                    message.dryRun = true;
                                    sender.send(message, regIds, function (err, result) {
                                        console.log(result);
                                        if (err) global.show_error(err, 500, res, true);
                                        else {
                                            res.json("ok");
                                            switchConversation(resp.document[0].username, req.query["receiver"], img_token);
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else
                    global.show_error("Bad token", 403, res, false);
            });
        } else
            global.show_error("Missing token", 404, res, false);
    });
});

function switchConversation(usr1, usr2, img) {
    var search_req = new cps.SearchRequest("\"" + usr1 + "\" \"" + usr2 + "\"");
    global.cps_connection.sendRequest(search_req, function (err, resp) {
        if (!err) {
            var isRead = resp.results.document[0].isRead;
            var mod = {
                sender: resp.results.document[0].sender,
                receiver: resp.results.document[0].receiver,
                isRead: isRead == "true" ? "false" : "true",
                image: img
            };

            var replace_request = new cps.PartialReplaceRequest(mod);
            global.cps_connection.sendRequest(replace_request, function(err, repl) {
                if (err)
                    console.error(err);
            });
        }
    });
}

module.exports = app;