var express = require('express');
var cps = require('cps-api');

var router = express.Router();

/* GET user listing. */
router.get('/', function(req, res, next) {
    var conn = global.cps_connection;
    if (req.query.hasOwnProperty("access_token")) {
        var token = req.query["access_token"];
        var search_req = new cps.SearchRequest("{" + cps.Term('"' + token + '"', "access_token") + "}");
        conn.sendRequest(search_req, function (err, resp) {
            if (err)
                global.show_error(err, 500, res, true);
            else if (resp.hasOwnProperty("results")) {
                var user = resp.results.document[0];
                var search_conv = new cps.SearchRequest("{" + cps.Term('"\"' + user.username + '\""') + "}");
                global.cps_conversation.sendRequest(search_conv, function(err2, resp2) {
                    if (err2)
                        global.show_error(err2, 500, res, true);
                    else {
                        console.log(user);
                        var conversations = resp2.results.document;
                        res.json({
                            username: user.username,
                            avatar: user.avatar,
                            friends: user.friends,
                            conversations: conversations
                        });
                    }
                });
            } else
                global.show_error("Bad token", 403, res, false);
        });
    } else
        global.show_error("Missing token", 404, res, false);
});

router.get('/*', function(req, res, next) {
    var conn = global.cps_connection;
    if (req.query.hasOwnProperty("access_token")) {
        var token = req.query["access_token"];
        var search_req = new cps.SearchRequest("{" + cps.Term('"' + token + '"', "access_token"));
        conn.sendRequest(search_req, function (err, resp) {
            if (err)
                    global.show_error(err, 500, res, false);
            else if (resp.hasOwnProperty("results")) {
                var search_req2 = new cps.SearchRequest("{" + cps.Term('"' + req.params[0]   + '"'));
                conn.sendRequest(search_req2, function (err2, resp2) {
                    console.log(req.params[0]); console.log(resp2);
                    if (err2)
                        global.show_error(err2, 404, res, true);
                    else {
                        var user = resp2.results.document[0];
                        res.json({
                            "username": user.username,
                            "avatar": user.avatar
                        })
                    }
                });
            } else
                global.show_error("Bad token", 403, res, false);
        });
    } else
        global.show_error("Missing token", 404, res, false);
});

module.exports = router;
