/**
 * Created by lopezjoe on 5/31/2015.
 */

/**
 * Created by lopezjoe on 5/31/2015.
 */
var express = require('express');
var cps = require('cps-api');
var multiparty = require('multiparty');
var mv = require('mv');
var uid = require('uid');

var router = express.Router();
var app = express();

app.get('/:id', function(req, res) {
    var conn = global.cps_connection;
    if (req.query.hasOwnProperty("access_token")) {
        var token = req.query["access_token"];
        var search_req = new cps.SearchRequest("{" + cps.Term('"' + token + '"', "access_token"));
        conn.sendRequest(search_req, function (err, resp) {
            if (err)
                global.show_error(err, 500, res, false);
            else if (resp.hasOwnProperty("results")) {
                res.sendFile('/data/avatars/' + req.params.id + '.jpg', {
                    root: __dirname + '/public',
                    dotfiles: 'deny',
                    headers: {
                        'x-timestamp': Date.now(),
                        'x-sent': true
                    }
                }, function (err) {
                    if (err) {
                        // TODO remove log
                        console.error(err);
                        res.status(404).end();
                    }
                });
            } else {
                global.show_error("Bad token", 403, res, false);
            }
        })
    } else
        global.show_error("Missing token", 403, res, false);
});

app.post('/', function(req, res) {
    var conn = global.cps_connection;
    if (req.query.hasOwnProperty("access_token")) {
        var token = req.query["access_token"];
        var search_req = new cps.SearchRequest("{" + cps.Term('"' + token + '"', "access_token") + "}");
        conn.sendRequest(search_req, function (err, resp) {
            if (err)
                global.show_error(err, 500, res, false);
            else if (resp.hasOwnProperty("results")) {
                var user = resp.results.document[0].username;
                var form = new multiparty.Form();
                form.parse(req, function (err, fields, files) {
                    if (err)
                        global.show_error(err, 500, res, true);
                    else if (!files)
                        global.show_error("No files in multipart", 500, res, true);
                    else {
                        var avatar = uid(5);
                        mv(files.avatar[0].path, __dirname + '/public/data/avatars/' + avatar + '.jpg', {mkdirp: true}, function (err) {
                            if (err)
                                global.show_error("Error moving image", 500, res, false);
                            else {
                                var upd = {
                                    username:user,
                                    access_token: token,
                                    avatar: avatar
                                };
                                var replace_request = new cps.PartialReplaceRequest(upd);
                                global.cps_connection.sendRequest(replace_request, function (err, repl) {
                                    if (err)
                                        global.show_error(err, 504, res, true);
                                    else
                                        res.json("ok");
                                });
                            }
                        });
                    }
                });
            } else
                global.show_error("Bad token", 403, res, false);
        });
    } else
        global.show_error("Missing token", 403, res, false);
});

module.exports = app;